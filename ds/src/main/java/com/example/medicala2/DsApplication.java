package com.example.medicala2;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Import;

import com.example.medicala2.WebSocket.WebSocketConfig;

@SpringBootApplication
@ComponentScan({"com.example.medicala2.WebSocket"})
@Import({WebSocketConfig.class})
public class DsApplication {

	public static void main(String[] args) {
		SpringApplication.run(DsApplication.class, args);
	}

}
