package com.example.medicala2.messageController;

import com.example.medicala2.Services.Interfaces.UserService;
import com.example.medicala2.messageProducer.MonitoredData;
import com.example.medicala2.messageProducer.Producer;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.stream.Collectors;

@Controller
public class RabbitController {

    private List<MonitoredData> monitoredData;
    private String fileName = "Activities.txt";

    @Autowired
    Producer producer;

    @Autowired
    private UserService userService;

    @GetMapping("/send")
    public String sendMessage(@RequestParam("msg") String msg){
        System.out.println("message "+msg);
        for (int i=0; i<25; i++){
            producer.produceMsg(msg);
        }
        return "Msg successfully sent";
    }

    @GetMapping("////")
    public void readFile(){


        try {
            monitoredData = Files.lines(Paths.get(fileName))
                    .map(line -> line.split("\t+"))
                    .map(x->{return new MonitoredData(DateTime.parse(x[0], DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss"))
                            , DateTime.parse(x[1], DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss"))
                            ,x[2]);})
                    .collect(Collectors.toList());
        } catch (IOException e) {
            e.printStackTrace();
        }
        for (MonitoredData i : monitoredData){
            producer.produceMsg(i.toString());
        }
    }


}
