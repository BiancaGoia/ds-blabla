package com.example.medicala2.Repo;

import com.example.medicala2.Entity.Beans.CaregivenPatient;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CaregivenPatientRepository extends JpaRepository<CaregivenPatient,Integer>{

}
