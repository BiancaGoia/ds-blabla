package com.example.medicala2.Repo;

import com.example.medicala2.Entity.Beans.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<User, Integer> {
}
