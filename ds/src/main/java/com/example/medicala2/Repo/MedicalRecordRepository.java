package com.example.medicala2.Repo;

import com.example.medicala2.Entity.Beans.MedicalRecord;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MedicalRecordRepository extends JpaRepository<MedicalRecord,Integer> {
}
