package com.example.medicala2.Repo;

import com.example.medicala2.Entity.Beans.MedicationPlan;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MedicationPlanRepository extends JpaRepository<MedicationPlan,Integer> {
}
