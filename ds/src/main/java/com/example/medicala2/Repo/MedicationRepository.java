package com.example.medicala2.Repo;

import com.example.medicala2.Entity.Beans.Medication;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MedicationRepository extends JpaRepository<Medication,Integer>{
}
