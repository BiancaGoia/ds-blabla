package com.example.medicala2.Services.Implementations;

import com.example.medicala2.Entity.Beans.Medication;
import com.example.medicala2.Entity.Beans.MedicationPlan;
import com.example.medicala2.Entity.Beans.User;
import com.example.medicala2.Repo.MedicalRecordRepository;
import com.example.medicala2.Repo.MedicationPlanRepository;
import com.example.medicala2.Repo.MedicationRepository;
import com.example.medicala2.Repo.UserRepository;
import com.example.medicala2.Services.Interfaces.MedicationPlanService;
import com.example.medicala2.Services.Interfaces.MedicationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.io.Serializable;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@Service
@Transactional
public class MedicationPlanServiceImpl implements MedicationPlanService,Serializable {

    @Autowired
    private MedicationPlanRepository medicationPlanRepository;
    @Autowired
    private MedicationRepository medicationRepository;
    @Autowired
    private UserRepository userRepository;

    public List<MedicationPlan> findBy(Map<String,String> allParams){
        MedicationPlan medicationPlan = setMedicationPlan(allParams);
        Example<MedicationPlan> example = Example.of(medicationPlan);

        //System.out.println(medicationPlanRepository.findAll(example)+"\n\n\n");
        return medicationPlanRepository.findAll(example);
    }

    public List<MedicationPlan> findAll(Map<String,String> allParams){
        return medicationPlanRepository.findAll();
    }

    public MedicationPlan modify(Map<String,String> allParams){
        MedicationPlan medicationPlan = setMedicationPlan(allParams);
        Example<MedicationPlan> example = Example.of(medicationPlan);

        switch (allParams.get("operation")){
            case "delete":
                medicationPlan = medicationPlanRepository.findAll(example).get(0);
                medicationPlanRepository.delete(medicationPlan);
                break;

            case "modify":
                //medicationPlan = medicationPlanRepository.findAll(example).get(0);
                medicationPlanRepository.save(medicationPlan);
                break;

            case "insert":
                medicationPlanRepository.save(medicationPlan);
        }
        return medicationPlan;
    }


    private MedicationPlan setMedicationPlan(Map<String,String> allParams){
        MedicationPlan medicationPlan = new MedicationPlan();



        if (allParams.get("id")!= null)
            medicationPlan.setId(Integer.parseInt(allParams.get("id")));

        //if (allParams.get("intakeIntervals")!= null)
            medicationPlan.setIntakeIntervals(allParams.get("intakeIntervals"));


        if(allParams.get("medicationId")!=null){
            Medication medication = medicationRepository.findById(Integer.parseInt(allParams.get("medicationId"))).get();
            medicationPlan.setMedicationId(medication);
        }
        //    medicationPlan.setMedicationId(Integer.parseInt(allParams.get("medicationId")));
        if(allParams.get("patientId")!=null){
            User user = userRepository.findById(Integer.parseInt(allParams.get("patientId"))).get();
            medicationPlan.setPatientId(user);
        }
         //   medicationPlan.setPatientId(Integer.parseInt(allParams.get("patientId")));
        //if (allParams.get("interval")!= null)
            medicationPlan.setPeriod(allParams.get("period"));

        System.out.println(medicationPlan.getId()+" "+medicationPlan.getPeriod()+" "+medicationPlan.getMedicationId()+"\n\n\n\n");
        return medicationPlan;
    }

}
