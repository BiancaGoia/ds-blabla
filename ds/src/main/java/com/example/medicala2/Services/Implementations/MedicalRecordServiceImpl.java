package com.example.medicala2.Services.Implementations;

import com.example.medicala2.Entity.Beans.MedicalRecord;
import com.example.medicala2.Entity.Beans.MedicationPlan;
import com.example.medicala2.Entity.Beans.User;
import com.example.medicala2.Repo.MedicalRecordRepository;
import com.example.medicala2.Repo.MedicationPlanRepository;
import com.example.medicala2.Repo.UserRepository;
import com.example.medicala2.Services.Interfaces.MedicalRecordService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Map;

@Service
@Transactional
public class MedicalRecordServiceImpl implements MedicalRecordService,Serializable {

    @Autowired
    private MedicalRecordRepository medicalRecordRepository;
    @Autowired
    private UserRepository userRepository;

    public List<MedicalRecord> findBy(Map<String,String> allParams){
        MedicalRecord medicalRecord = setMedicalRecord(allParams);
        Example<MedicalRecord> example = Example.of(medicalRecord);

        return medicalRecordRepository.findAll(example);
    }

    public MedicalRecord modify(Map<String,String> allParams){
        MedicalRecord medicalRecord = setMedicalRecord(allParams);
        Example<MedicalRecord> example = Example.of(medicalRecord);

        switch (allParams.get("operation")){
            case "delete":
                medicalRecord = medicalRecordRepository.findAll(example).get(0);
                medicalRecordRepository.delete(medicalRecord);
                break;

            case "modify":
                medicalRecord = medicalRecordRepository.findAll(example).get(0);
                medicalRecordRepository.save(medicalRecord);
                break;

            case "insert":
                medicalRecordRepository.save(medicalRecord);
        }
        return medicalRecord;
    }


    private MedicalRecord setMedicalRecord(Map<String,String> allParams){
        MedicalRecord medicalRecord = new MedicalRecord();

        if (allParams.get("id")!= null)
            medicalRecord.setId(Integer.parseInt(allParams.get("id")));

        medicalRecord.setObservation(allParams.get("observation"));
        if(allParams.get("patientId")!=null){
            User user = userRepository.findById(Integer.parseInt(allParams.get("patientId"))).get();
            medicalRecord.setPatientId(user);
        }
        if(allParams.get("doctorId")!=null){
            User user = userRepository.findById(Integer.parseInt(allParams.get("doctorId"))).get();
            medicalRecord.setPatientId(user);
        }
        //medicalRecord.setDoctorId(Integer.parseInt(allParams.get("doctorId")));
        //medicalRecord.setPatientId(Integer.parseInt(allParams.get("patientId")));

        if (allParams.get("operation").equals("insert")) {
            Date date = new Date();
            medicalRecord.setDate(date);
        }

        return medicalRecord;
    }


}
