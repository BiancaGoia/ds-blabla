package com.example.medicala2.Services.Interfaces;

import com.example.medicala2.Entity.Beans.User;

import java.util.List;
import java.util.Map;

public interface UserService {

    List<User> findBy(Map<String,String> allParams);

    User modify(Map<String,String> allParams);

    //just for testing purposes

    List<User> findAll();
}
