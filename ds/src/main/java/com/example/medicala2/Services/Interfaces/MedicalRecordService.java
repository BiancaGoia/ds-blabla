package com.example.medicala2.Services.Interfaces;

import com.example.medicala2.Entity.Beans.MedicalRecord;
import com.example.medicala2.Entity.Beans.Medication;

import java.util.List;
import java.util.Map;

public interface MedicalRecordService {

    List<MedicalRecord> findBy(Map<String,String> allParams);

    MedicalRecord modify(Map<String,String> allParams);
}
