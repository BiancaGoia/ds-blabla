package com.example.medicala2.Services.Implementations;

import com.example.medicala2.Entity.Beans.User;
import com.example.medicala2.Entity.Enums.UserGender;
import com.example.medicala2.Entity.Enums.UserProfile;
import com.example.medicala2.Repo.UserRepository;
import com.example.medicala2.Services.Interfaces.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
@Transactional
public class UserServiceImpl implements UserService, Serializable {

    @Autowired
    private UserRepository userRepository;

    private BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder();

    public List<User> findAll(){
        return userRepository.findAll();
    }

    public List<User> findBy(Map<String, String> allParams) {
        User user = setUser(allParams);
        Example<User> example = Example.of(user);
        if (allParams.get("password") != null) {
            user = userRepository.findAll(example).get(0);
            //if (bCryptPasswordEncoder.matches(allParams.get("password"), user.getPassword())) {
                return userRepository.findAll(example);
            //} else
             //   return null;
        } else {
            return userRepository.findAll(example);
        }

    }

    public User modify(Map<String, String> allParams) {
        User user = setUser(allParams);
        Example<User> userExample = Example.of(user);

        System.out.println(user);
        if (allParams.get("operation").equals("delete")){
            user=userRepository.findAll(userExample).get(0);
            userRepository.delete(user);
            System.out.println("deleted \n");
            System.out.println(user);
            return user;
        }
        else
            return userRepository.save(user);

    }

    private User setUser(Map<String,String> allParams){
        User user = new User();


        if (allParams.get("id")!=null){
            user.setId(Integer.parseInt(allParams.get("id")));
        }
        else
        {
            user.setId(null);
        }

        user.setUsername(allParams.get("username"));
        user.setBirthdate(allParams.get("birthdate"));
        user.setAddress(allParams.get("address"));

        //if ((allParams.get("password")!=null)&&(allParams.get("operation")!=null))
           // user.setPassword(bCryptPasswordEncoder.encode(allParams.get("password")));
        if(allParams.get("password")!=null){
            user.setPassword(allParams.get("password"));
        }

        user.setBirthdate(allParams.get("birthdate"));
        user.setName(allParams.get("name"));

        if (allParams.get("gender")!=null){
            if (allParams.get("gender").equals("MALE")) {
                user.setGender(UserGender.MALE);
            }
            else {
                user.setGender(UserGender.FEMALE);
            }
        }

        if (allParams.get("profile")!=null) {
            if (allParams.get("profile").equals("DOCTOR")) {
                user.setProfile(UserProfile.DOCTOR);
            } else if (allParams.get("profile").equals("CAREGIVER")) {
                user.setProfile(UserProfile.CAREGIVER);
            }
            else if (allParams.get("profile").equals("PATIENT"))
            {
                user.setProfile(UserProfile.PATIENT);
            }
        }

        return user;
    }

}
