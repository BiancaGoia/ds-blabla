package com.example.medicala2.Services.Interfaces;

import com.example.medicala2.Entity.Beans.Medication;

import java.util.List;
import java.util.Map;

public interface MedicationService {

    List<Medication> findBy(Map<String,String> allParams);

    Medication modify(Map<String,String> allParams);
}
