package com.example.medicala2.Services.Implementations;

import com.example.medicala2.Entity.Beans.CaregivenPatient;
import com.example.medicala2.Entity.Beans.MedicalRecord;
import com.example.medicala2.Entity.Beans.User;
import com.example.medicala2.Repo.CaregivenPatientRepository;
import com.example.medicala2.Repo.MedicalRecordRepository;
import com.example.medicala2.Repo.UserRepository;
import com.example.medicala2.Services.Interfaces.CaregivenPatientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Map;

@Service
@Transactional
public class CaregivenPatientServiceImpl implements CaregivenPatientService, Serializable{

    @Autowired
    private CaregivenPatientRepository caregivenPatientRepository;

    @Autowired
    private UserRepository userRepository;

    public List<CaregivenPatient> findBy(Map<String,String> allParams){
        CaregivenPatient caregivenPatient = setCaregivenPatient(allParams);
        Example<CaregivenPatient> example = Example.of(caregivenPatient);

        return caregivenPatientRepository.findAll(example);
    }

    public CaregivenPatient modify(Map<String,String> allParams){
        CaregivenPatient caregivenPatient = setCaregivenPatient(allParams);
        Example<CaregivenPatient> example = Example.of(caregivenPatient);

        switch (allParams.get("operation")){
            case "delete":
                caregivenPatient = caregivenPatientRepository.findAll(example).get(0);
                caregivenPatientRepository.delete(caregivenPatient);
                break;

            case "modify":
                caregivenPatient = caregivenPatientRepository.findAll(example).get(0);
                caregivenPatientRepository.save(caregivenPatient);
                break;

            case "insert":
                caregivenPatientRepository.save(caregivenPatient);
        }
        return caregivenPatient;
    }


    private CaregivenPatient setCaregivenPatient(Map<String,String> allParams){
        CaregivenPatient caregivenPatient = new CaregivenPatient();

        if (allParams.get("id")!= null)
            caregivenPatient.setId(Integer.parseInt(allParams.get("id")));
        if (allParams.get("caregiverId")!= null){
            User caregiver = userRepository.findById(Integer.parseInt(allParams.get("caregiverId"))).get();
            caregivenPatient.setCaregiverId(caregiver);
        }
           // caregivenPatient.setCaregiverId(Integer.parseInt(allParams.get("caregiverId")));
        //if (allParams.get("patientId")!= null)
          //  caregivenPatient.setPatientId(Integer.parseInt(allParams.get("patientId")));
            if(allParams.get("patientId")!=null){
                User user = userRepository.findById(Integer.parseInt(allParams.get("patientId"))).get();
                caregivenPatient.setPatientId(user);
            }

        return caregivenPatient;
    }

}
