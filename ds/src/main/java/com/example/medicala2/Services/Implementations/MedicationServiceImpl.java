package com.example.medicala2.Services.Implementations;


import com.example.medicala2.Entity.Beans.Medication;
import com.example.medicala2.Entity.Beans.User;
import com.example.medicala2.Repo.MedicationRepository;
import com.example.medicala2.Repo.UserRepository;
import com.example.medicala2.Services.Interfaces.MedicationService;
import com.example.medicala2.Services.Interfaces.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
@Transactional
public class MedicationServiceImpl implements MedicationService, Serializable {

    @Autowired
    private MedicationRepository medicationRepository;


    public List<Medication> findBy(Map<String,String> allParams){
        Medication medication = setMedication(allParams);
        Example<Medication> example = Example.of(medication);

        return medicationRepository.findAll(example);
    }

    public Medication modify(Map<String,String> allParams){
        Medication medication = setMedication(allParams);
        Example<Medication> example = Example.of(medication);

        switch (allParams.get("operation")){
            case "delete":
                    medication = medicationRepository.findAll(example).get(0);
                    medicationRepository.delete(medication);
                break;

            case "modify":
                    //medication = medicationRepository.findAll(example).get(0);
                    medicationRepository.save(medication);
                    break;

            case "insert":
                    medicationRepository.save(medication);
        }
        return medication;
    }


    private Medication setMedication(Map<String,String> allParams){
        Medication medication = new Medication();

        if (allParams.get("id")!= null)
            medication.setId(Integer.parseInt(allParams.get("id")));

        //if(allParams.get("dosage")!=null)
            medication.setDosage(allParams.get("dosage"));

        //if(allParams.get("name")!=null)
            medication.setName(allParams.get("name"));

        //if(allParams.get("sideEffect")!=null)
            medication.setSideEffect(allParams.get("sideEffect"));
        System.out.println(medication.getId()+" "+medication.getName()+" "+medication.getSideEffect()+" "+medication.getDosage()+"\n\n");
        return medication;
    }
}
