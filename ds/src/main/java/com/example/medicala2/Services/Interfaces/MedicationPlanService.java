package com.example.medicala2.Services.Interfaces;

import com.example.medicala2.Entity.Beans.Medication;
import com.example.medicala2.Entity.Beans.MedicationPlan;

import java.util.List;
import java.util.Map;

public interface MedicationPlanService {

    List<MedicationPlan> findBy(Map<String,String> allParams);

    MedicationPlan modify(Map<String,String> allParams);

    List<MedicationPlan> findAll(Map<String,String> allParams);
}
