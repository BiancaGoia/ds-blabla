package com.example.medicala2.Services.Interfaces;

import com.example.medicala2.Entity.Beans.CaregivenPatient;
import com.example.medicala2.Entity.Beans.Medication;

import java.util.List;
import java.util.Map;

public interface CaregivenPatientService {

    List<CaregivenPatient> findBy(Map<String,String> allParams);

    CaregivenPatient modify(Map<String,String> allParams);

}
