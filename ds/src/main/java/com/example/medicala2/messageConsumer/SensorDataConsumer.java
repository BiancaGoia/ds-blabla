package com.example.medicala2.messageConsumer;

import com.example.medicala2.messageProducer.SensorData;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessageListener;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class SensorDataConsumer{

    @Autowired
    private SimpMessagingTemplate messagingTemplate;

    @RabbitListener(queues = "${rabbitmq.queue}", containerFactory="jsaFactory")
    public void onMessage(SensorData message){
        //byte[] body = message.getBody();
        //System.out.println("Received message "+ message.toString());

        checkRules(message);

        messagingTemplate.convertAndSend("/topic", message);
    }



    private void checkRules(SensorData message){

        DateTime startTime = DateTime.parse(message.getStartTime(), DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss"));
        DateTime endTime = DateTime.parse(message.getEndTime(), DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss"));

        int sleepOrLeave = 8;
        int toileting = 1 ;
        switch (message.getActivity()){
            case "Sleeping":
                    if (((endTime.getMillis()-startTime.getMillis())-sleepOrLeave*3600000)>0) {
                        System.out.println("Patient with id " + message.getPatientId() + " was sleeping for too long");
                    }
                break;
            case "Leaving":
                if (((endTime.getMillis()-startTime.getMillis())-sleepOrLeave*3600000)>0) {
                    System.out.println("Patient with id " + message.getPatientId() + " was out for too long");
                }
                break;
            case "Toileting":
                if (((endTime.getMillis()-startTime.getMillis())-toileting*3600000)>0) {
                    System.out.println("Patient with id " + message.getPatientId() + " was toileting for too long");
                }
                break;
            default:
                break;
        }

    }
}
