package com.example.medicala2.Controller;

import com.example.medicala2.Entity.Beans.MedicationPlan;
import com.example.medicala2.Entity.Beans.User;
import com.example.medicala2.Entity.Enums.UserProfile;
import com.example.medicala2.Services.Interfaces.MedicationPlanService;
import com.example.medicala2.Services.Interfaces.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
public class MedicationPlanController {

    @Autowired
    private MedicationPlanService medicationPlanService;

    private HttpSession session;

    @RequestMapping(value = "medicationPlan/findall", method = RequestMethod.POST)
    @ResponseBody
    public List<MedicationPlan> findAll(@RequestParam Map<String,String> allParams, HttpServletRequest request) {


        return medicationPlanService.findAll(allParams);

    }

    @RequestMapping(value = "medicationPlan/find", method = RequestMethod.POST)
    @ResponseBody
    public List<MedicationPlan> findAllBy(@RequestParam Map<String,String> allParams, HttpServletRequest request) {
        request.getSession();
        session = request.getSession();
        User user = (User) session.getAttribute("user");

        if (user.getProfile().equals(UserProfile.DOCTOR)) {
            return medicationPlanService.findBy(allParams);
        }
        else
        {
            if (user.getProfile().equals(UserProfile.PATIENT)) {

                Map<String, String> map = new HashMap<String, String>();
                map.put("patientId", user.getUsername());
                return medicationPlanService.findBy(allParams);
            }
            else
                return null;
        }
    }

    @RequestMapping(value = "medicationPlan/modify", method = RequestMethod.POST)
    @ResponseBody
    public MedicationPlan modifyMedicationPlan(@RequestParam Map<String,String> allParams,HttpServletRequest request){
        request.getSession();
        session=request.getSession();
        User user = (User) session.getAttribute("user");
        if (user.getProfile().equals(UserProfile.DOCTOR)) {
            return medicationPlanService.modify(allParams);

        }
        else
            return null;

    }
}