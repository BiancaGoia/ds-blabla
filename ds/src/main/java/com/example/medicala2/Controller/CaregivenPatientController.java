package com.example.medicala2.Controller;

import com.example.medicala2.Entity.Beans.CaregivenPatient;
import com.example.medicala2.Entity.Beans.MedicationPlan;
import com.example.medicala2.Entity.Beans.User;
import com.example.medicala2.Entity.Enums.UserProfile;
import com.example.medicala2.Services.Interfaces.CaregivenPatientService;
import com.example.medicala2.Services.Interfaces.MedicationPlanService;
import com.example.medicala2.Services.Interfaces.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
public class CaregivenPatientController {

    @Autowired
    private CaregivenPatientService caregivenPatientService;

    private HttpSession session;


    @RequestMapping(value = "caregiven/find", method = RequestMethod.POST)
    @ResponseBody
    public List<CaregivenPatient> findAllBy(@RequestParam Map<String,String> allParams, HttpServletRequest request) {
        request.getSession();
        session = request.getSession();
        User user = (User) session.getAttribute("user");

        if (user.getProfile().equals(UserProfile.DOCTOR)) {
            return caregivenPatientService.findBy(allParams);
        }
        else {
            if (user.getProfile().equals(UserProfile.CAREGIVER)) {
                Map<String, String> map = new HashMap<String, String>();
                map.put("caregiverId", user.getUsername());
                return caregivenPatientService.findBy(allParams);

            }
            else
            {
                return null;
            }
        }
    }

    @RequestMapping(value = "caregiven/modify", method = RequestMethod.POST)
    @ResponseBody
    public CaregivenPatient modifyCaregivenPatient(@RequestParam Map<String,String> allParams,HttpServletRequest request){
        request.getSession();
        session=request.getSession();
        User user = (User) session.getAttribute("user");
        if (user.getProfile().equals(UserProfile.DOCTOR)) {
            return caregivenPatientService.modify(allParams);

        }
        else
            return null;

    }
}