package com.example.medicala2.Controller;

import com.example.medicala2.Entity.Beans.MedicalRecord;
import com.example.medicala2.Entity.Beans.MedicationPlan;
import com.example.medicala2.Entity.Beans.User;
import com.example.medicala2.Entity.Enums.UserProfile;
import com.example.medicala2.Services.Interfaces.MedicalRecordService;
import com.example.medicala2.Services.Interfaces.MedicationPlanService;
import com.example.medicala2.Services.Interfaces.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.List;
import java.util.Map;

@Controller
public class MedicalRecordController {

    @Autowired
    private MedicalRecordService medicalRecordService;

    private HttpSession session;


    @RequestMapping(value = "medicalRecord/find", method = RequestMethod.POST)
    @ResponseBody
    public List<MedicalRecord> findAllBy(@RequestParam Map<String,String> allParams, HttpServletRequest request) {
        request.getSession();
        session = request.getSession();
        User user = (User) session.getAttribute("user");

        if (user.getProfile().equals(UserProfile.DOCTOR)) {
            return medicalRecordService.findBy(allParams);
        }
        else
        {
            return null;
        }
    }

    @RequestMapping(value = "medicalRecord/modify", method = RequestMethod.POST)
    @ResponseBody
    public MedicalRecord modifyMedicalRecord(@RequestParam Map<String,String> allParams,HttpServletRequest request){
        request.getSession();
        session=request.getSession();
        User user = (User) session.getAttribute("user");
        if (user.getProfile().equals(UserProfile.DOCTOR)) {
            return medicalRecordService.modify(allParams);

        }
        else
            return null;

    }
}