package com.example.medicala2.Controller.Filters;

import com.example.medicala2.Entity.Beans.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.*;
import javax.servlet.FilterConfig;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;


public class LoginFilter implements Filter {

	private static final Logger LOGGER = LoggerFactory.getLogger(LoginFilter.class);

	public static final String LOGIN_PAGE = "/";

	@Override
	public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain)
			throws IOException, ServletException {

		HttpServletRequest httpServletRequest = (HttpServletRequest) servletRequest;
		HttpServletResponse httpServletResponse = (HttpServletResponse) servletResponse;

		User user = (User) httpServletRequest.getSession().getAttribute("user");
		boolean loggedIn = user != null;
		boolean loginRequest = httpServletRequest.getRequestURI().equals(LOGIN_PAGE);
		boolean loginCheck = httpServletRequest.getRequestURI().equals("/logIn");
		boolean staticContent2 = httpServletRequest.getRequestURI().equals("/topbar");

		System.out.println(httpServletRequest.getRequestURI());
		System.out.println(httpServletRequest.getRequestURI().equals(LOGIN_PAGE));
		System.out.println(httpServletRequest.getRequestURI().equals("/logIn"));
		System.out.println(httpServletRequest.getRequestURI().equals("/topbar"));
		System.out.println(loggedIn);
//		System.out.println(user.getUsername());


		if (loggedIn || loginRequest || loginCheck  || staticContent2) {
			System.out.println("user logged");
			filterChain.doFilter(servletRequest, servletResponse);
		} else {
			try {
				throw new Exception("no user logged in");
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	@Override
	public void init(FilterConfig arg0) throws ServletException {
		LOGGER.debug("loginFilter initialized");
	}

	@Override
	public void destroy() {
		// close resources
	}
}