package com.example.medicala2.Controller;

import com.example.medicala2.Entity.Beans.User;
import com.example.medicala2.Entity.Enums.UserProfile;
import com.example.medicala2.Services.Interfaces.UserService;
import com.example.medicala2.messageProducer.MonitoredData;
import com.example.medicala2.messageProducer.Producer;
import com.example.medicala2.messageProducer.SensorData;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.stream.Collectors;

@Controller
public class UserController {

    private List<SensorData> sensorData;
    private String fileName = "Activities.txt";

    @Autowired
    private UserService userService;

    @Autowired
    Producer producer;

    private HttpSession session;

    @RequestMapping(value = "/logIn", method = RequestMethod.POST)
    @ResponseBody
    public User logIn(@RequestParam Map<String,String> allParams, HttpServletRequest request) {
        request.getSession();

        System.out.println(allParams);
        User user = userService.findBy(allParams).get(0);
        if (user!=null) {
            session =request.getSession();
            session.setAttribute("user", user);
            request.getSession();
            request.setAttribute("user", user);


        }
        return user;

    }

    @RequestMapping(value = "/", method = RequestMethod.GET)
    @ResponseBody
    public void read(HttpServletRequest request){

        Map<String, String> map = new HashMap<String, String>();
        map.put("profile", "PATIENT");
        List<User> patients =  userService.findBy(map);
        try {
            sensorData = Files.lines(Paths.get(fileName))
                    .map(line -> line.split("\t+"))
                    .map(x->{return new SensorData(0,x[0],x[1],x[2]);})
                    .collect(Collectors.toList());
        } catch (IOException e) {
            e.printStackTrace();
        }

        for (SensorData d : sensorData){
            int rnd = new Random().nextInt(patients.size());
            d.setPatientId(patients.get(rnd).getId());
            producer.produceObjMsg(d);
        }

    }

    @RequestMapping(value="logOut",method = RequestMethod.GET)
    @ResponseBody
    public void logOut(HttpServletRequest request) {
        request.getSession().setAttribute("user",null);
        session=request.getSession();
        session.removeAttribute("user");
        session.invalidate();
    }

    @RequestMapping(value = "user/find", method = RequestMethod.POST)
    @ResponseBody
    public List<User> findAllBy(@RequestParam Map<String,String> allParams, HttpServletRequest request) {
        request.getSession();
        session = request.getSession();
        User user = (User) session.getAttribute("user");

        if (user.getProfile().equals(UserProfile.DOCTOR)) {
           return userService.findBy(allParams);
        }
        else
        {
            if (user.getProfile().equals(UserProfile.PATIENT)) {

                Map<String, String> map = new HashMap<String, String>();
                map.put("username", user.getUsername());
                return userService.findBy(map);
            }
            else
                return null;
        }
    }

    @RequestMapping(value = "user/all", method = RequestMethod.GET)
    @ResponseBody
    public List<User> findAll(){
        return userService.findAll();
    }


    @RequestMapping(value = "user/modify", method = RequestMethod.POST)
    @ResponseBody
    public User modifyUser(@RequestParam Map<String,String> allParams,HttpServletRequest request){
        request.getSession();
        session=request.getSession();
        User user = (User) session.getAttribute("user");
        if (user.getProfile().equals(UserProfile.DOCTOR)) {
            System.out.println("\navem un doctor\n");
            return userService.modify(allParams);

        }
        else
            return null;

    }
}