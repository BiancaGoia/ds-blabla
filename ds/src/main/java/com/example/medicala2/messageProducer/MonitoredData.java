package com.example.medicala2.messageProducer;

import org.joda.time.DateTime;
import org.joda.time.Seconds;
import org.joda.time.format.DateTimeFormat;

public class MonitoredData {
    private DateTime startTime;
    private DateTime endTime;
    private String activity;

    public MonitoredData(DateTime a,DateTime b, String c){
        this.startTime=a;
        this.endTime=b;
        this.activity =c;
    }

    public long getShort(){
        if (getDifference()<300){
            return 1;
        }
        return 0;
    }

    public long getDifference() {
        Seconds s=Seconds.secondsBetween(startTime,endTime);
        String a=s.toString();
        long i=Integer.parseInt(a.substring(2,a.length()-1));
        return i;
    }

    public String getStartData(){

        org.joda.time.format.DateTimeFormatter dtfOut = DateTimeFormat.forPattern("yyyy-MM-dd");
        return (dtfOut.print(getStartTime()));
    }

    public String getActivity() {
        return this.activity;
    }

    public DateTime getEndTime() {
        return endTime;
    }

    public DateTime getStartTime() {
        return startTime;
    }

    public void setActivity(String activity) {
        this.activity = activity;
    }

    public void setEndTime(DateTime endTime) {
        this.endTime = endTime;
    }

    public void setStartTime(DateTime startTime) {
        this.startTime = startTime;
    }

    @Override
    public String toString() {
        return (this.startTime+" "+this.endTime+" "+this.activity);
    }
}
