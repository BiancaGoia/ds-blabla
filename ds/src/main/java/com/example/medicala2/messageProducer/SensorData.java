package com.example.medicala2.messageProducer;


public class SensorData {

    private Integer patientId;
    private String startTime;
    private String endTime;
    private String activity;

    public SensorData(Integer patientId, String startTime, String endTime, String activity) {
        this.patientId = patientId;
        this.startTime = startTime;
        this.endTime = endTime;
        this.activity = activity;
    }

    public SensorData(){

    }

    public Integer getPatientId() {
        return patientId;
    }

    public void setPatientId(Integer patientId) {
        this.patientId = patientId;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public String getActivity() {
        return activity;
    }

    public void setActivity(String activity) {
        this.activity = activity;
    }

    @Override
    public String toString() {
        return (this.patientId+ " " + this.startTime+" "+this.endTime+" "+this.activity);
    }

}
