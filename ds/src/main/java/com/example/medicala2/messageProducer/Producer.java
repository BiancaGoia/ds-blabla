package com.example.medicala2.messageProducer;

import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class Producer {

    @Autowired
    private AmqpTemplate amqpTemplate;

    @Value("${rabbitmq.queue}")
    private String queueName;

    @Value("${rabbitmq.exchange}")
    private String exchange;

    @Value("${rabbitmq.routingkey}")
    private String routingKey;

    public void produceMsg(String msg){
        amqpTemplate.convertAndSend(exchange,routingKey,msg);
        System.out.println("Send msg = "+msg);
    }

    public void produceObjMsg(SensorData o ){
        amqpTemplate.convertAndSend(exchange,routingKey,o);
        System.out.println("Send msg = "+o.toString());
    }
}
