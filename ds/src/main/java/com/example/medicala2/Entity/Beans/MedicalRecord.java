package com.example.medicala2.Entity.Beans;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "medicalRecord")
public class MedicalRecord extends GeneralBean implements Serializable {

    @ManyToOne//(cascade=CascadeType.REMOVE)
    @JoinColumn(name = "patientId", referencedColumnName = "id")
    private User patientId;

    @ManyToOne//(cascade=CascadeType.REMOVE)
    @JoinColumn(name = "doctorId", referencedColumnName = "id")
    private User doctorId;

    @Column
    private Date date;

    @Column
    private String observation;

    public MedicalRecord(){

    }

    public MedicalRecord(User patientId, User doctorId, Date date, String observation) {
        this.patientId = patientId;
        this.doctorId = doctorId;
        this.date = date;
        this.observation = observation;
    }

    public User getPatientId() {
        return patientId;
    }

    public void setPatientId(User patientId) {
        this.patientId = patientId;
    }

    public User getDoctorId() {
        return doctorId;
    }

    public void setDoctorId(User doctorId) {
        this.doctorId = doctorId;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getObservation() {
        return observation;
    }

    public void setObservation(String observation) {
        this.observation = observation;
    }
}
