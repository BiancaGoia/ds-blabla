package com.example.medicala2.Entity.Enums;

public enum UserGender {
    MALE,
    FEMALE
}
