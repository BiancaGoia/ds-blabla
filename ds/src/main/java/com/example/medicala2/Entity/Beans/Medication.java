package com.example.medicala2.Entity.Beans;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Table(name = "medication")
public class Medication extends GeneralBean implements Serializable {

    @Column
    private String name;

    @Column
    private String sideEffect;

    @Column
    private String dosage;

    public Medication(){

    }

    public Medication(String name, String sideEffect, String dosage) {
        this.name = name;
        this.sideEffect = sideEffect;
        this.dosage = dosage;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSideEffect() {
        return sideEffect;
    }

    public void setSideEffect(String sideEffect) {
        this.sideEffect = sideEffect;
    }

    public String getDosage() {
        return dosage;
    }

    public void setDosage(String dosage) {
        this.dosage = dosage;
    }
}
