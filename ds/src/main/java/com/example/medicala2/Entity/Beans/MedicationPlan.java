package com.example.medicala2.Entity.Beans;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "medicationPlan")
public class MedicationPlan extends GeneralBean implements Serializable {

    @ManyToOne
    @JoinColumn(name = "medicationId", referencedColumnName = "id")
    private Medication medicationId;

    @ManyToOne
    @JoinColumn(name = "patientId", referencedColumnName = "id")
    private User patientId;

    @Column
    private String intakeIntervals;

    @Column
    private String period;

    public MedicationPlan(){

    }

    public MedicationPlan(Medication medicationId, User patientId, String intakeIntervals, String period) {
        this.medicationId = medicationId;
        this.patientId = patientId;
        this.intakeIntervals = intakeIntervals;
        this.period = period;
    }

    public Medication getMedicationId() {
        return medicationId;
    }

    public void setMedicationId(Medication medicationId) {
        this.medicationId = medicationId;
    }

    public User getPatientId() {
        return patientId;
    }

    public void setPatientId(User patientId) {
        this.patientId = patientId;
    }

    public String getIntakeIntervals() {
        return intakeIntervals;
    }

    public void setIntakeIntervals(String intakeIntervals) {
        this.intakeIntervals = intakeIntervals;
    }

    public String getPeriod() {
        return period;
    }

    public void setPeriod(String period) {
        this.period = period;
    }
}
