package com.example.medicala2.Entity.Enums;

public enum UserProfile {
    DOCTOR,
    CAREGIVER,
    PATIENT
}
