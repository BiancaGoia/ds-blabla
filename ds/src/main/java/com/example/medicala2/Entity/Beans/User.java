package com.example.medicala2.Entity.Beans;

import com.example.medicala2.Entity.Enums.UserGender;
import com.example.medicala2.Entity.Enums.UserProfile;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "user")
public class User extends GeneralBean implements Serializable{

    @Column(unique = true)
    private String username;

    @Column
    private String password;

    @Column
    private String name;

    @Column
    private String birthdate;

    @Column
    @Enumerated(EnumType.STRING)
    private UserProfile profile;

    @Column
    private String address;

    @Column
    @Enumerated(EnumType.STRING)
    private UserGender gender;

    public User(){

    }

    public User(String username, String password, String name, String birthdate, UserProfile profile, String address, UserGender gender) {
        this.username = username;
        this.password = password;
        this.name = name;
        this.birthdate = birthdate;
        this.profile = profile;
        this.address = address;
        this.gender = gender;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBirthdate() {
        return birthdate;
    }

    public void setBirthdate(String birthdate) {
        this.birthdate = birthdate;
    }

    public UserProfile getProfile() {
        return profile;
    }

    public void setProfile(UserProfile profile) {
        this.profile = profile;
    }

    public UserGender getGender() {
        return gender;
    }

    public void setGender(UserGender gender) {
        this.gender = gender;
    }
}
