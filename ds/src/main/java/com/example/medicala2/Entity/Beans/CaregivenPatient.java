package com.example.medicala2.Entity.Beans;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "caregivenPatient")
public class CaregivenPatient extends GeneralBean implements Serializable {

    @ManyToOne
    @JoinColumn(name = "caregiverId", referencedColumnName = "id")
    private User caregiverId;

    @ManyToOne
    @JoinColumn(name = "patientId", referencedColumnName = "id")
    private User patientId;

    public CaregivenPatient(){

    }

    public CaregivenPatient(User caregiverId, User patientId) {
        this.caregiverId = caregiverId;
        this.patientId = patientId;
    }

    public User getCaregiverId() {
        return caregiverId;
    }

    public void setCaregiverId(User caregiverId) {
        this.caregiverId = caregiverId;
    }

    public User getPatientId() {
        return patientId;
    }

    public void setPatientId(User patientId) {
        this.patientId = patientId;
    }
}
